<?php

namespace App\Http\Controllers\Detalles;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Detalles;

class DetallesController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $detalle=$request->input('det');
        $factura=$request->input('foliof');
        $producto=$request->input('producto');
        $cantidad=$request->input('cantidad');
        $precioventa=$request->input('precio_unitario');
        $detalle_id=Detalles::guardar($detalle,$factura,$producto,$cantidad,$precioventa);
        return redirect()->route('Pedido.mostrar',['id'=>$factura]);
        #app('App\Http\Controllers\Pedidos\PedidosController')->show($factura); expose the forms resend info
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $factura_id=Detalles::findOrFail($id);
        #dd($factura_id->toArray());
        Detalles::eliminar($id);
        return redirect()->route('Pedido.mostrar',['id'=>$factura_id->foliof]);
    }
}
