<?php

namespace App\Http\Controllers\Pedidos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Facturas;
use App\Models\Detalles;
use App\Models\Productos;

class PedidosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id=1)
    {
       $facturas = Facturas::Where('status', $id)->get();
       if ($id==1) {
         $msg = 'Facturas pendientes';
       }else {
         $msg = 'Factuas pagadas';
       }
        return view('Pedidos.index')
        ->with('facturas', $facturas)
        ->with('title', $msg);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Pedidos.guardar')
        ->with('title', 'Nuevo pedido');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $total;
        $estatus;
        $titular;
        $folio=$request->input('folio');
        if ($folio!=0) {
          if ($request->input('fin')!=null) {
            $titular=self::getTitular($folio);
          }else {
            $titular=$request->input('titular');
          }
        }
        else {
            $titular= $request->input('titular');
        }
        if ($request->input('total')==null) {
          $total=0;
        }
        else {
          $total = $request->input('total');
        }
        if ($request->input('status')==null) {
          $estatus=1;
        }
        else {
          $estatus = $request->input('status');
        }

        $factura_id=Facturas::guardar($folio,$titular,$total,$estatus);
        return redirect()->route('Pedido.mostrar',['id'=>$factura_id]);
    }

    public function getTitular($folio)
    {
      $factura=Facturas::findOrFail($folio);
      return $factura->titular;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $factura=Facturas::findOrFail($id);
      $detalles=$factura->Detalles;
      $productos=Productos::all();
        return view('Pedidos.Detalle')
        ->with('title','Detalles de factura')
        ->with('factura',$factura)
        ->with('detalles', $detalles)
        ->with('productos', $productos);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Facturas::Eliminar($id);
      return redirect()->route('Pedido.inicio');
    }
    public function calcularTotal($id)
    {
      $factura=Facturas::findOrFail($id);
      $total=0;
      $detalles=$factura->Detalles;
      foreach ($detalles as $detalle) {
        $total= $total + ($detalle->cantidad * $detalle->precio_unitario);
      }
      return $total;
    }
}
