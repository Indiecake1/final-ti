<?php

namespace App\Http\Controllers\Productos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Productos;

class ProductosController extends Controller
{
    /**
     * Funcion para guardar/editar los registros pertenecientes a la tabla de produtos
     * @param Request $request Datos enviados por el formulario mediante el metodo POST
     */
    public function Guardar(Request $request)
    {
      $id=$request->input('prod');
      $producto=$request->input('producto');
      $precio=$request->input('precio');
      Productos::guardar($id,$producto,$precio);
      return redirect()->route('iniProd');
    }

    public function eliminar($id)
    {
      Productos::Eliminar($id);
      return redirect()->route('iniProd');
    }


    public function productosini()
    {
      return view('tbl.productos')
      ->with('productos', Productos::all())
      ->with('title', 'Reporte general de productos');
    }
}
