<?php

namespace App\Http\Controllers\Reportes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Facturas;
use Maatwebsite\Excel\Facades\Excel;

class ReportesController extends Controller
{
    public function sumario()
    {
      return view('Reportes.sumario')
      ->with('facturas', Facturas::where('status',2)->paginate(5))
      ->with('granTotal', self::ValorTotal());
    }

    public function expSumario()
    {
      Excel::create('Reporte sumario', function($excel) {

            $excel->sheet('Datos',function($sheet) {

                //header
                $sheet->setBorder('A1:D1','thin');
                $sheet->setBorder('A2:D2','thin');
                $sheet->mergeCells('A1:D1');
                $sheet->row(1, ['Reporte por sumario']);
                $sheet->mergeCells('A2:C2');
                $sheet->row(2,['Total generado:','','','$ '.number_format(self::ValorTotal(),2,'.',',')]);
                $sheet->cells("A2:D2",function($cells){
                  $cells->setBackground('#ffcc99');
                });

                $n=3;

                $fila=[
                    0 => '',
                    1 => '',
                    2 => '',
                    3 => '',
                ];
                $sheet->appendRow($fila);
                $n++;

                //data
                $datos=Facturas::where('status',2)->get();
                $detalles;

                $sheet->setOrientation('landscape');
                //$sheet->fromArray($datos);
                foreach ($datos as $dato) {
                    $colu=[
                        0 => 'Factura',
                        1 => 'Titular',
                        2 => 'Fecha',
                        3 => 'Total',
                    ];
                    $sheet->appendRow($colu);
                    $sheet->setBorder("A{$n}:D{$n}",'thin');
                    $sheet->cells("A{$n}:D{$n}",function($cells){
                      $cells->setBackground('#67b2ff');##99ccff
                    });

                    $n++;#

                    $row=[
                        0 => $dato->folio,
                        1 => $dato->titular,
                        2 => $dato->fecha,
                        3 => '$'.number_format($dato->total,2,'.',','),
                    ];
                    $allOrdenes=$dato->Detalles;
                    $sheet->appendRow($row);
                    $sheet->setBorder("A{$n}:D{$n}",'thin');
                    $n++;

                    //$n++;
                    $tip=[
                        0 => 'Orden',
                        1 => 'Producto',
                        2 => 'Precio por unidad',
                        3 => 'Cantidad',
                    ];
                    $sheet->appendRow($tip);
                    $sheet->setBorder("A{$n}:D{$n}",'dashed');
                      /*  $sheet->cells("A{$n}:F{$n}",function($cells){
                        $cells->setBackground('#9999FF');
                      }); */
                    $n++;#
                    foreach ($allOrdenes as $orden) {

                        $fila=[
                            0 => $orden->det,
														1 => $orden->Producto->producto,
                            2 => '$'.number_format($orden->precio_unitario,2,'.',','),
														3 => $orden->cantidad,
                        ];

                        $sheet->appendRow($fila);
                        $sheet->setBorder("A{$n}:D{$n}",'thin');
                        $n++;#
                    }
                    $n+$dato->Detalles->count();
                    $fila=[
                        0 => '',
                        1 =>'',
                        2 => 'Total',
                        3 => '$'.number_format(self::calcularTotal($dato),2,'.',','),
                    ];
                    $sheet->appendRow($fila);
                    $sheet->setBorder("C{$n}:D{$n}",'thin');
                    $sheet->cells("C{$n}:D{$n}",function($cells){
                      $cells->setBackground('#99ccff');##67b2ff
                    });
                    $n++;#

                    $fila=[
                        0 => '',
                        1 => '',
                        2 => '',
                        3 => '',
                    ];
                    $sheet->appendRow($fila);
                    $n++;
                }
            });
        })->export('xls');
    }

    public function ValorTotal()
    {
        $granTotal=0;
        $facturas=Facturas::where('status',2)->get();
        foreach ($facturas as $factura) {
           $granTotal=$granTotal+self::calcularTotal($factura);
        }
        return $granTotal;
    }

    public function calcularTotal(Facturas $factura)
    {
        $total=0;
        $detalles=$factura->Detalles;
        foreach ($detalles as $detalle) {
          $total= $total + ($detalle->cantidad * $detalle->precio_unitario);
        }
        return $total;
    }
}
