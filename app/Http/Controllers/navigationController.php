<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Facturas;

class navigationController extends Controller
{
  public function index()
  {
      $pendientes = Facturas::where('status',1)->count();
      return view('welcome')
      ->with('title','Inicio')
      ->with('nPedidos',$pendientes);
  }
}
