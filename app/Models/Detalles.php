<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Detalles extends Model
{
  //protected $connection = 'ferre';
  protected $table = 'detalles';
  protected $primaryKey ='det';
  public $fillable= [
    'foliof','prodf','cantidad','precio_unitario'
  ];
  public $timestamps=false;
  use SoftDeletes;
  public function Producto()
  {
    return $this->belongsTo('App\Models\Productos', 'prodf')->withTrashed();
  }
  public function Factura()
  {
    return $this->belongsTo('App\Models\Facturas','foliof');
  }
  public function scopeGuardar($query,$detalle,$factura,$producto,$cantidad,$precioventa)
  {
    if ($detalle==0) {
      $documento = new Detalles();
    }else {
      $documento = Detalles::findOrFail($detalle);
    }
    $documento->foliof=$factura;
    $documento->prodf=$producto;
    $documento->cantidad=$cantidad;
    $documento->precio_unitario=$precioventa;
    $documento->save();
    return $documento->det;
  }
  public function scopeEliminar($query,$detalle)
  {
    $documento = Detalles::findOrFail($detalle);
    $documento->delete();
  }
}
