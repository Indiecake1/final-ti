<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FacturaStatus extends Model
{
    protected $table = 'facturas_estatus';
    protected $primaryKey = 'fact_est_id';

    public $timestamps = false;
}
