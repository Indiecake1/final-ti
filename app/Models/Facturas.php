<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Facturas extends Model
{
  //protected $connection = 'ferre';
  protected $table = 'facturas';
  protected $primaryKey ='folio';
  public $fillable= [
    'titular','fecha','status'
  ];
  use SoftDeletes;
  public $timestamps=false;

  protected $attributes = [
    'status' => 1,
  ];

  public function Detalles()
  {
    return $this->hasMany('App\Models\Detalles', 'foliof');
  }

  public function Estatus()
  {
    return $this->hasOne('App\Models\FacturaStatus', 'fact_est_id', 'status');
  }

  public function scopeGuardar($query,$folio,$titular,$total,$estatus)
  {
    if ($folio==0) {
      $documento = new Facturas();
    }else {
      $documento= Facturas::findOrFail($folio);
    }
    $documento->titular=$titular;
    $documento->fecha=date('Y-m-d');
    $documento->total=$total;
    $documento->status=$estatus;

    $documento->save();
    return $documento->folio;
  }
  public function scopeEliminar($scope,$folio)
  {
    $documento = Facturas::findOrFail($folio);
    $documento->delete();
  }
}
