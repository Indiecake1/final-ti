<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Productos extends Model
{
    //protected $connection = 'ferre';
    protected $table = 'productos';
    protected $primaryKey ='prod';
    public $fillable= [
      'producto','precio'
    ];
    public $timestamps=false;
    use SoftDeletes;

    public function scopeListado($query)
    {
      $datos = $query
      ->select('*')
      ->get()->toArray();
      $datos = (sizeof($datos) > 0)?$datos:array();
      return $datos;
    }

    public function Detalles()
    {
      return $this->hasMany('App\Models\Detalles', 'prodf');
    }

    public function scopeGuardar($query,$id,$producto,$precio)
    {
      if ($id==0) {
        $documento = new Productos();
      }else {
        $documento = Productos::findOrFail($id);
      }
      $documento->producto=$producto;
      $documento->precio=$precio;
      $documento->save();
      return $documento->prod;
    }

    public function scopeEliminar($query,$producto)
    {
      $documento = Productos::findOrFail($producto);
      $documento->delete();
    }
}
