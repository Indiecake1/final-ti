<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Ciudades::class, function (Faker $faker) {
    return [
        'ciudad_dms'=>$faker->city(),
    ];
});
