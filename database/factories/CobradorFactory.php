<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Cobradores::class, function (Faker $faker) {
    return [
        'nombre_dms' => $faker->name,
        'apaterno_dms' => $faker->lastName,#$faker->unique()->safeEmail,
        'amaterno_dms' => $faker->lastName,# now(),
        'calle_dms' => $faker->streetName, // secret
        'colonia_dms' =>$faker->firstName, #str_random(10),
        'nun_casa_dms' =>$faker->randomDigit,
        'ciuf_dms'=>$faker->numberBetween($min =1, $max=4), #$faker->city,
        'telefono_dms'=> $faker->phoneNumber,
        'comision_dms'=>$faker->randomFloat($nbMaxDecimals = 2, $min = 1000, $max = 3000),
        'fotografia_dms'=>$faker->numberBetween($min =1, $max=15).'d.jpg',
    ];
});
