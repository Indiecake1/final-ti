<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Detalles::class, function (Faker $faker) {
    return [
        'foliof_dms'=>$faker->numberBetween($min =1, $max=20),
        'prodf_dms'=>$faker->numberBetween($min =1, $max=35),
        'cantidad_dms'=>$faker->numberBetween($min =1, $max=3),#$faker->randomDigit(),
        'preciovta_dms'=>$faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 350),
    ];
});
