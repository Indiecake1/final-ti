<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Facturas::class, function (Faker $faker) {
    return [
      'fecha_dms'=> $faker->date($format = 'Y-m-d', $max = 'now'),
      'clif_dms'=> $faker->numberBetween($min =1, $max=15),
      'descuento_dms'=> $faker->numberBetween($min =100, $max=499),
      'status_dms'=> $faker->randomElement($array = array ('Pendiente','Cobrada')),
      'acredito_dms'=> $faker->boolean(),
    ];
});
