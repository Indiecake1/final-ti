<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Pagos::class, function (Faker $faker) {
    return [
        'cobf_dms'=>$faker->numberBetween($min =1, $max=15),
        'fechapago_dms'=>$faker->date($format = 'Y-m-d', $max = 'now'),
        'horapago_dms'=>$faker->time($format = 'H:i:s', $max = 'now'),
        'importe_dms'=>$faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 70),
    ];
});
