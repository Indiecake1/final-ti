<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Productos::class, function (Faker $faker) {
    return [
        'producto_dms'=> $faker->unique()->word(),
        'precio_dms'=> $faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 300),
        'existencia_dms'=>$faker->numberBetween($min =500, $max=1600),
        'reorden_dms'=>$faker->numberBetween($min =1, $max=499),
        'fotografia_dms'=>$faker->numberBetween($min =1, $max=35).'p.jpg',
    ];
});
