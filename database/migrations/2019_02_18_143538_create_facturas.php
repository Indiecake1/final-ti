<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturas', function (Blueprint $table) {
            $table->increments('folio');
            $table->string('titular');
            $table->date('fecha');
            $table->float('total', 8, 2);
            $table->unsignedInteger('status');
            $table->softDeletes();
            /*$table->foreign('clif')
              ->references('cli')
              ->on('clientes')
              ->onDelete('cascade');
              */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturas');
    }
}
