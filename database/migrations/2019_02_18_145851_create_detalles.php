<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalles', function (Blueprint $table) {
            $table->increments('det');
            $table->unsignedInteger('foliof');
            $table->unsignedInteger('prodf');
            $table->unsignedInteger('cantidad');
            $table->unsignedDecimal('precio_unitario', 15, 2);
            $table->softDeletes();
            /*
              ->references('folio')
              ->on('facturas')
              ->onDelete('cascade');
            $table->foreign('prodf')
              ->references('prod')
              ->on('productos')
              ->onDelete('cascade');*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalles');
    }
}
