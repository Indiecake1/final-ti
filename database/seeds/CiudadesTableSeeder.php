<?php

use Illuminate\Database\Seeder;
use App\Models\Ciudades;

class CiudadesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Ciudades::truncate();
      factory(Ciudades::class)->times(4)->create();
    }
}
