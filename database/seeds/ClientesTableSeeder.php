<?php

use Illuminate\Database\Seeder;
use App\Models\Clientes;

class ClientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Clientes::truncate();
        factory(Clientes::class)->times(15)->create();
    }
}
