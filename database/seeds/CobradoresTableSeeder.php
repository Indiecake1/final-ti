<?php

use Illuminate\Database\Seeder;
use App\Models\Cobradores;

class CobradoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cobradores::truncate();
        factory(Cobradores::class)->create([
          'nombre_dms' =>'Tonny',
          'apaterno_dms'=>'Montana',
          'comision_dms'=>'1000',
          'fotografia_dms'=>'td.jpg',
        ]);
        factory(Cobradores::class)->times(14)->create();
    }
}
