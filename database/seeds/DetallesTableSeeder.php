<?php

use Illuminate\Database\Seeder;

class DetallesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\Detalles::truncate();
        factory(App\Models\Detalles::class)->times(35)->create();
    }
}
