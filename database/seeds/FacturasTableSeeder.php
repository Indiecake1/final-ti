<?php

use Illuminate\Database\Seeder;

class FacturasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\Facturas::truncate();
        factory(App\Models\Facturas::class)->times(20)->create();
    }
}
