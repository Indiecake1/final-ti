<?php

use Illuminate\Database\Seeder;

class PagosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\Pagos::truncate();
        factory(App\Models\Pagos::class)->times(35)->create();
    }
}
