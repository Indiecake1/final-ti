<?php

use Illuminate\Database\Seeder;
use App\Models\Productos;

class ProductosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Productos::truncate();
      factory(Productos::class)->create([
        'producto_dms'=>'Paquete plumas',
        'fotografia_dms'=>'12p.jpg',
      ]);#1
      factory(Productos::class)->create([
        'producto_dms'=>'Lote libretas',
        'fotografia_dms'=>'16p.jpg',
      ]);#2
      factory(Productos::class)->create([
        'producto_dms'=>'Archivador',
        'fotografia_dms'=>'4p.jpg',
      ]);#3
      factory(Productos::class)->create([
        'producto_dms'=>'lote de carpetas',
        'fotografia_dms'=>'13p.jpg',
      ]);#4
      factory(Productos::class)->create([
        'producto_dms'=>'Sobres',
        'fotografia_dms'=>'7p.jpg',
      ]);#5
      factory(Productos::class)->create([
        'producto_dms'=>'Borrador',
        'fotografia_dms'=>'19p.jpg',
      ]);#6
      factory(Productos::class)->create([
        'producto_dms'=>'Lapicera',
        'fotografia_dms'=>'1p.jpg',
      ]);#7
      factory(Productos::class)->create([
        'producto_dms'=>'Libretas universitarias',
        'fotografia_dms'=>'2p.jpg',
      ]);#8
      factory(Productos::class)->create([
        'producto_dms'=>'Tabloide minerales',
        'fotografia_dms'=>'3p.jpg',
      ]);#9
      factory(Productos::class)->create([
        'producto_dms'=>'Maceta',
        'fotografia_dms'=>'5p.jpg',
      ]);#10
      factory(Productos::class)->create([
        'producto_dms'=>'Pluma cactus',
        'fotografia_dms'=>'6p.jpg',
      ]);#11
      factory(Productos::class)->create([
        'producto_dms'=>'Bolsa para lapices',
        'fotografia_dms'=>'8p.jpg',
      ]);#12
      factory(Productos::class)->create([
        'producto_dms'=>'Pluma fuente',
        'fotografia_dms'=>'9p.jpg',
      ]);#13
      factory(Productos::class)->create([
        'producto_dms'=>'kit geometrico',
        'fotografia_dms'=>'11p.jpg',
      ]);#14
      factory(Productos::class)->create([
        'producto_dms'=>'Pluma decorada',
        'fotografia_dms'=>'14p.jpg',
      ]);#15
      factory(Productos::class)->create([
        'producto_dms'=>'Libreta Stars wars',
        'fotografia_dms'=>'15p.jpg',
      ]);#16
      factory(Productos::class)->create([
        'producto_dms'=>'Punta repuesto para pluma',
        'fotografia_dms'=>'17p.jpg',
      ]);#17
      factory(Productos::class)->create([
        'producto_dms'=>'Lote pluma cactus',
        'fotografia_dms'=>'18p.jpg',
      ]);#18
      factory(Productos::class)->create([
        'producto_dms'=>'Corrector cinta',
        'fotografia_dms'=>'19p.jpg',
      ]);#19
      factory(Productos::class)->create([
        'producto_dms'=>'Papelera',
        'fotografia_dms'=>'20p.jpg',
      ]);#20
      factory(Productos::class)->create([
        'producto_dms'=>'Expendedor de cinta',
        'fotografia_dms'=>'21p.jpg',
      ]);#21
      factory(Productos::class)->create([
        'producto_dms'=>'Borradores perro',
        'fotografia_dms'=>'34p.jpg',
      ]);#22
      factory(Productos::class)->create([
        'producto_dms'=>'Agenda 2019',
        'fotografia_dms'=>'35p.jpg',
      ]);#23
      factory(Productos::class)->times(25)->create();
    }
}
