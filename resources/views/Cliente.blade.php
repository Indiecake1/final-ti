@extends('template.dashboard')
@section('title','Reporte compuesto de clientes')
@section('pageTitle',$title)
  @section('breadcrumb')
    <li class="breadcrumb-item active" aria-current="page">Ranking</li>
  @endsection
@php
  $table ='clientes'
@endphp
@section('content')
<div class="card">
  <div class="card-body">
    <h5 class="card-title">Top clientes</h5>
    <table id="tabla_{{$table}}" class="table table-bordered">
      <thead>
        <tr>
          <td>Fotografia</td>
          <td>Nombre</td>
          <td>Colonia</td>
          <td>Limite de credito</td>
          <td>Dinero Generado</td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><img width="30%" height="30%" class="rounded-circle" src="{{asset('img')}}/{{ $mejorCliente->fotografia_dms }}"></td>
          <td>{{ $mejorCliente->nombre_dms }} {{ $mejorCliente->apaterno_dms }} {{ $mejorCliente->amaterno_dms }}</td>
          <td>{{ $mejorCliente->colonia_dms }}</td>
          <td>{{ number_format($mejorCliente->limcredito_dms, 2, '.', ',' ) }} $</td>
          <td>{{ number_format($mejorCliente->total, 2, '.', ',' ) }} $</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>

<div class="card">
  <div class="card-body">
    <h5 class="card-title">El producto más fructuoso</h5>
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>Id</th>
          <th>Fotografia</th>
          <th>Producto</th>
          <th>Existencia</th>
          <th>Reorden</th>
          <th>Solicitud al proveedor</th>
          <th>Dinero otorgado</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>{{ $mejorproducto->prod_dms }}</td>
          <td><img width="30%" class="rounded-circle" src="{{asset('img')}}/{{ $mejorproducto->fotografia_dms }}"></td>
          <td>{{ $mejorproducto->producto_dms }}</td>
          <td>{{ $mejorproducto->existencia_dms }}</td>
          <td>{{ $mejorproducto->reorden_dms }}</td>
          <td>{{ $mejorproducto->existencia_dms-$mejorproducto->reorden_dms }}</td>
          <td>{{ number_format($mejorproducto->total, 2, '.', ',' ) }} $</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
@endsection
@section('scripts')

  <script type="text/jscript">
/*
  $(document).ready(function() {
  var table{{--$table}} = $('#tabla_{{$table--}}').DataTable({
      responsive: true,
      dom: 'Bfrtip',
       buttons:[
           {extend: 'csv',
           exportOptions: {
           columns: ':visible'
           }},
           {extend: 'excel',
           exportOptions: {
           columns: ':visible'
           }},
           {extend: 'pdf',
           exportOptions: {
           columns: ':visible'
           }},
           {extend: 'print',
            exportOptions: {
            columns: ':visible'
            }},
           {extend: 'colvis',
           text: 'Columnas'
           },
           ],
           select: true,
           columnDefs: [ {
         } ],
            "responsive": true,
            "serverSide": true,
            "paging": true,
            "language": {
            "info": "Página _PAGE_ a _PAGES_",
            "infoEmpty": "Sin resultados",
            "sLengthMenu": "Mostrar _MENU_ resultados",
            "sSearch" : "Buscar",
            "sZeroRecords" : 'Sin resultados',
            "scrollX": true,
            "scrollCollapse": true,
            "paginate": {
                "next": "Siguiente",
                "previous": "Anterior"
            }
    },
        "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "Todos"]],
        "ajax": {
            "url": "{{--asset('/Reporte/clienteVta')--}}",
            "type": "POST",
            "data" : {_token:"{{-- csrf_token() --}}"}
        },
        "columns": [
                { "data": "fotografia_dms", "orderable": false, "className": "text-center", "render" : function(data, type, full, meta){
                    return '<img  class="btn btn-secondary waves-effect waves-light m-1" href="{{--asset('img')--}}/'+data+'">';
                } },
                { "data": "cliente","className": "text-justify", "orderable": true },
                { "data": "colonia_dms","className": "text-justify", "orderable": false },
                { "data": "limcredito_dms","className": "text-justify", "orderable": false },
                { "data": "colonia_dms","className": "text-justify", "orderable": false }
        ]
    });
  });
*/
  </script>


@endsection
