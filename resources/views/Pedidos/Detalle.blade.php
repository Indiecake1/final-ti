@extends('template.dashboard')
@section('title','Pedidos pendientes')
@section('pageTitle',$title)
  @section('breadcrumb')
    <li class="breadcrumb-item active" aria-current="page"><a href=" {{ Route('Pedido.inicio',['status'=>'1'])}}">Pedidos</a></li>
    <li class="breadcrumb-item active" aria-current="page">detalles</li>
  @endsection

@inject('control', 'app\Http\Controllers\Pedidos\PedidosController')

@section('content')
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
          <h5 class="card-title">Información de la factura # {{ $factura->folio }} || {{ $factura->fecha }}</h5>
          <hr>
          <div class="form-group row">
            <label for="fname" class="col-sm-2 text-right control-label col-form-label">Folio:</label>
            <div class="col-sm-9">
              <input type="text" readonly="true" value="{{ $factura->folio }}" class="form-control">

            </div>
        </div>
        <div class="form-group row">
            <label for="lname" class="col-sm-2 text-right control-label col-form-label">Titular:</label>
            <div class="col-sm-9">
              <input type="text" readonly="true" value="{{ $factura->titular }}" class="form-control">

            </div>
        </div>
        <div class="form-group row">
            <label for="lname" class="col-sm-2 text-right control-label col-form-label">Fecha de expedición:</label>
            <div class="col-sm-9">
              <input type="text" readonly="true" value="{{ $factura->fecha }}" class="form-control">

            </div>
        </div>
        <div class="form-group row">
            <label for="email1" class="col-sm-2 text-right control-label col-form-label">Total:</label>
            <div class="col-sm-9">
              <input type="text" readonly="true" value="${{ number_format($factura->total, 2,'.',',') }}" class="form-control">

            </div>
        </div>
        <div class="form-group row">
            <label for="cono1" class="col-sm-2 text-right control-label col-form-label">Estado de la factura</label>
            <div class="col-sm-9">
              <input type="text" readonly="true" value="{{ $factura->Estatus->estado }}" class="form-control">

            </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Detalles de la factura</h5>
        <hr>
        <div class="form-group text-right">
          @if ($factura->status==1)
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#nuevoDet"><i class="mdi mdi-library-plus"></i></button>
          @else
            <br>
          @endif
        </div>

        <div class="modal fade" id="nuevoDet" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="">Agregar nueva orden</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              </div>
              <div class="modal-body">
                <form class="" action="{{ route('Detalle.guardar') }}" method="post">
                  @csrf
                  <input type="text" hidden="true" name="det" value="0">
                  <input type="text" hidden=true name="foliof" value="{{ $factura->folio }}">
                  <div class="form-group">
                    <label for="prodf">Producto</label>
                    <select class="form-control" id="prodf" name="producto" data-select2-id="1">
                      <option disabled selected value="0">Selecciona una opción</option>
                      @foreach($productos as $producto)
                                <option value="{{ isset($producto->prod)?$producto->prod:0 }}">{{ isset($producto->producto)?$producto->producto:''}}</option>
                        @endforeach
                    </select>

                  </div>
                  <div class="form-group">
                    <label for="cantidad">Cantidad</label>
                    <input type="number" required class="form-control" min="1" id="cantidad" name="cantidad" placeholder="Seleccione la cantidad">
                  </div>

                  <div class="form-group">
                    <label for="preciovta">Precio por unidad del producto</label>
                    <input type="text" required class="form-control" id="preciovta" name="precio_unitario" placeholder="Ingrese el precio del producto">
                  </div>


              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-success">Confirmar</button>
                </form>
              </div>
            </div>
          </div>
        </div>

        <div class="responsive-table">
          <table class="table table-hover">
            <thead>
              <tr>
                <td>Folio</td>
                <td>Factura</td>
                <td>Producto</td>
                <td>cantidad</td>
                <td>Precio por unidad</td>
                @if ($factura->status==1)
                  <td>Opciones</td>
                @endif
              </tr>
            </thead>
            <tbody>
              @foreach ($detalles as $detalle)
                <tr>
                  <td>{{ $detalle->det }}</td>
                  <td>{{ $detalle->foliof}} || {{$detalle->Factura->fecha}}</td>
                  <td>{{ $detalle->Producto->producto }}</td>
                  <td>{{ $detalle->cantidad }}</td>
                  <td>${{ number_format($detalle->precio_unitario, 2,'.',',' ) }} </td>
                  @if ($factura->status==1)
                    <td>
                      <button data-toggle="modal" data-target="#editarDet{{$detalle->det}}" class="btn btn-outline-warning"><i class="mdi mdi-lead-pencil"></i></button>
                      <button data-toggle="modal" data-target="#borrar{{$detalle->det}}" class="btn btn-outline-danger"><i class="mdi mdi-delete"></i></button>
                    </td>
                  @endif
                </tr>

                <div class="modal fade" id="editarDet{{$detalle->det}}" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title" id="">Editar la orden N°{{ $detalle->det }}</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      </div>
                      <div class="modal-body">
                        <form class="" action="{{ route('Detalle.guardar') }}" method="post">
                          @csrf
                          <input type="text" hidden="true" name="det" value="{{ $detalle->det }}">
                          <input type="text" hidden=true name="foliof" value="{{ $factura->folio }}">
                          <div class="form-group">
                            <label for="prodf">Producto</label>
                            <select class="form-control" id="prodf" name="producto" data-select2-id="1">
                              <option disabled value="0">Selecciona una opción</option>
                              @foreach($productos as $producto)
                                        <option value="{{ isset($producto->prod)?$producto->prod:0 }}" @if ($detalle->prodf==$producto->prod) selected @endif>{{ isset($producto->producto)?$producto->producto:''}}</option>
                                @endforeach
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="cantidad">Cantidad</label>
                            <input type="number" required class="form-control" min="1" id="cantidad" name="cantidad" value="{{ $detalle->cantidad }}" placeholder="Seleccione la cantidad">
                          </div>
                          <div class="form-group">
                            <label for="preciovta">Precio por unidad del producto</label>
                            <input type="text" required class="form-control" id="preciovta" name="precio_unitario" value="{{ $detalle->precio_unitario }}" placeholder="Ingrese el precio del producto">
                          </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-success">Confirmar</button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>


                <div class="modal fade" id="borrar{{$detalle->det}}" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title" id="¿Borrar registro?">¿Borrar registro?</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      </div>
                      <div class="modal-body">
                        Estas por Eliminar el producto: <strong>{{$detalle->Producto->producto}}</strong> de la orden.
                      </div>
                      <form class="" action="{{ route('Detalle.borrar', ['id'=> $detalle->det]) }}" method="post">
                        @csrf
                        {{ method_field('DELETE') }}
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                          <button type="submit" class="btn btn-danger">Borrar</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              @endforeach
            </tbody>
          </table>
        </div>

        <div class="col-md-12">
          <div class="pull-right m-t-30 text-right">
            <hr>
            <h3><b>Total :</b> ${{ number_format($control->calcularTotal($factura->folio),2,'.',',') }}</h3>
          </div>
          <hr>
        </div>
        @if ($factura->status==1)
          <div class="text-right">
            <form class="form-horizontal" action="{{ Route('Pedido.guardar') }}" method="post">
              @csrf

              <input type="text" name="folio" value="{{ $factura->folio }}" hidden="true">
              <input type="number" hidden="true" name="total" value="{{ $control->calcularTotal($factura->folio) }}">
              <input type="text" name="status" hidden="true" value="2">
              <input type="text" name="fin" hidden value="1">
              <button class="btn btn-danger" type="submit"> Terminar el pedido </button>
            </form>
          </div>
        @endif
      </div>
    </div>
  </div>
@endsection

@section('scripts')

@endsection
