@extends('template.dashboard')
@section('title','Nuevo pedido')
@section('pageTitle',$title)
  @section('breadcrumb')
    <li class="breadcrumb-item active" aria-current="page"><a href=" {{ Route('Pedido.inicio',['status'=>'1'])}}">Pedidos</a></li>
    <li class="breadcrumb-item active" aria-current="page">detalles</li>
  @endsection

@section('content')
  <div class="card">
    <form class="form-horizontal">
      <div class="card-body">
          <h4 class="card-title">Información del pedido</h4>
          <hr>
          <div class="form-group row">
              <label for="fname" class="col-sm-2 text-right control-label col-form-label">titular</label>
              <div class="col-sm-9">
                  <input type="text" class="form-control" name="titular" placeholder="Ingrese su nombre">
              </div>
          </div>
          <div class="form-group row">
              <label for="lname" class="col-sm-2 text-right control-label col-form-label">Fecha</label>
              <div class="col-sm-9">
                  <input type="text"  class="form-control mydatepicker" name="fecha" value="{{date('Y-m-d')}}">
              </div>
          </div>

      </div>
      <div class="border-top">
          <div class="card-body">
              <button type="button" class="btn btn-primary">Enviar</button>
          </div>
      </div>
    </form>
  </div>
@endsection

@section('scripts')
<script>
/*datwpicker*/
    jQuery('.mydatepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });
    var quill = new Quill('#editor', {
        theme: 'snow'
    });
</script>
@endsection
