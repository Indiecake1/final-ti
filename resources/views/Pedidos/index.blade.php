@extends('template.dashboard')
@section('title', $title)
@section('pageTitle', $title)
  @section('breadcrumb')
    <li class="breadcrumb-item active" aria-current="page">Pedidos</li>
  @endsection

@section('content')
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">{{ $title }}</h5>
        <hr>
        <div class="form-group text-right">
          <button type="button" class="btn btn-success" data-toggle="modal" data-target="#nuevoPedido"><i class="mdi mdi-library-plus"></i></button>
        </div>
        <div class="responsive-table">
          <table class="table table-bordered">
            <thead>
              <tr>
                <td>Folio</td>
                <td>Titular</td>
                <td>Fecha</td>
                <td>Estado del pedido</td>
                <td>Opciones</td>
              </tr>
            </thead>
            <tbody>
              @foreach ($facturas as $factura)
                <tr>
                  <td>{{ $factura->folio }}</td>
                  <td>{{ $factura->titular}}</td>
                  <td>{{ $factura->fecha }}</td>
                  <td>{{ $factura->Estatus->estado }}</td>
                  <td>
                    <a href="{{ route('Pedido.mostrar', ['id'=> $factura->folio]) }}" class="btn btn-outline-primary"><i class="mdi mdi-eye-outline"></i></a>
                    <button  data-toggle="modal" data-target="#editar{{$factura->folio}}" class="btn btn-outline-warning"><i class="mdi mdi-lead-pencil"></i></button>
                    <button data-toggle="modal" data-target="#borrar{{$factura->folio}}" class="btn btn-outline-danger"><i class="mdi mdi-delete"></i></button>
                  </td>
                </tr>

                <div class="modal fade" id="editar{{$factura->folio}}" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title" id="info">Editar el pedido <strong>#{{ $factura->folio }}</strong></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      </div>
                      <form class="form-horizontal" action="{{ route('Pedido.guardar') }}" method="post">
                        @csrf
                        <input type="text" hidden="true" name="folio" value="0">
                        <div class="modal-body">
                          <div class="form-group row">
                            <label for="fname" class="col-sm-2 text-right control-label col-form-label">titular</label>
                            <div class="col-sm-9">
                              <input type="text" required class="form-control" id="fname" name="titular" value="{{$factura->titular}}" placeholder="Ingrese su nombre">
                            </div>
                          </div>
                          <input type="text" name="folio" hidden value="{{$factura->folio}}">
                          <input type="text" hidden name="status" value="{{$factura->status}}">
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                          <button type="submit" class="btn btn-success">Enviar</button>

                        </div>
                      </form>
                    </div>
                  </div>
                </div>

              <div class="modal fade" id="borrar{{$factura->folio}}" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title" id="¿Borrar registro?">¿Borrar registro?</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                      Estas por Eliminar el pedido con el folio <strong>N°{{$factura->folio}}</strong>
                    </div>
                    <form class="" action="{{ route('Pedido.borrar', ['folio'=> $factura->folio]) }}" method="post">
                      @csrf
                      {{ method_field('DELETE') }}
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-danger">Borrar</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>


              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="nuevoPedido" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="info">Nuevo Pedido</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <form class="form-horizontal" action="{{ route('Pedido.guardar') }}" method="post">
        @csrf
        <input type="text" hidden="true" name="folio" value="0">
        <div class="modal-body">
          <div class="form-group row">
            <label for="fname" class="col-sm-2 text-right control-label col-form-label">titular</label>
            <div class="col-sm-9">
              <input type="text" required class="form-control" id="fname" name="titular" placeholder="Ingrese su nombre">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-success">Enviar</button>

        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('scripts')

@endsection
