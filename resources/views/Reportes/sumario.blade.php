@extends('template.dashboard')
@section('title','Reporte sumario')
@section('pageTitle','Reporte de sumario')
  @section('breadcrumb')
    <li class="breadcrumb-item active" aria-current="page">Reportes</li>
    <li class="breadcrumb-item active" aria-current="page">Sumario</li>
  @endsection

@section('content')
<div class="col-md-12">
  <div class="text-right">
    <div class="form-group">
      <a href="{{ Route('Reporte.sumario.exportar') }}" class="btn btn-success"> <i class="mdi mdi-file-excel"></i> Exportar a Excel</a>
    </div>
  </div>
</div>
@foreach ($facturas as $factura)
  <div class="row">
    <div class="col-md-12">
        <div class="card card-body printableArea">
            <h4><b>Factura</b> <span class="pull-right">#{{ $factura->folio }}</span></h4>
            <hr>
            <div class="row">
                <div class="col-md-12">
                  <address>
                    <p><b>Titular de la factura : </b>{{ $factura->titular }}</p>
                    <p><b>Fecha de la factura : </b> <i class="fa fa-calendar"></i>  {{ $factura->fecha }}</p>
                  </address>
                </div>
                <div class="col-md-12">
                    <div class="table-responsive m-t-40" style="clear: both;">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th>Producto</th>
                                    <th class="text-right">Cantidad</th>
                                    <th class="text-right">Precio por unidad</th>
                                </tr>
                            </thead>
                            <tbody>
                              @foreach ($factura->Detalles as $detalle)
                                <tr>
                                    <td class="text-center">{{ $detalle->det }}</td>
                                    <td> {{ $detalle->Producto->producto }} </td>
                                    <td class="text-right"> {{ $detalle->cantidad }} </td>
                                    <td class="text-right"> {{ $detalle->precio_unitario }} </td>
                                </tr>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="clearfix"></div>
                    <div class="text-right">
                      <h4>Total: $ {{number_format($factura->total,2,'.',',')}} </h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
@endforeach

<div class="row">
  <div class="col-md-12">
    <div class="card card-body printableArea">
      {{ $facturas->links() }}
      <hr>
      <h4><b>Total generado</b> <span class="pull-right"> ${{ number_format($granTotal,2,'.',',') }} </span></h4>
      <hr>
    </div>
  </div>
</div>

@endsection
