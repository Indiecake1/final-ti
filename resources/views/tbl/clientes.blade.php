@extends('template.dashboard')
@section('title','Reporte de clientes')
@section('pageTitle',$title)
  @section('breadcrumb')
    <li class="breadcrumb-item active" aria-current="page">Clientes</li>
  @endsection

@section('content')
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Todos los clientes</h5>
        <div class="responsive-table">
          <table class="table table-bordered">
            <thead>
              <tr>
                <td>Fotografia</td>
                <td>Nombre</td>
                <td>Calle</td>
                <td>Colonia</td>
                <td>Numero de casa</td>
                <td>Ciudad</td>
                <td>Limite de credito</td>
              </tr>
            </thead>
            <tbody>
              @foreach ($clientes as $cliente)
                <tr>
                  <td><img width="30%" class="rounded-circle" src="{{asset('img')}}/{{ $cliente->fotografia_dms }}"></td>
                  <td>{{ $cliente->nombre_dms }} {{ $cliente->apaterno_dms }} {{ $cliente->amaterno_dms }}</td>
                  <td>{{ $cliente->calle_dms}}</td>
                  <td>{{ $cliente->colonia_dms }}</td>
                  <td>{{ $cliente->nun_casa_dms}}</td>
                  <td>{{ $cliente->Ciudad->ciudad_dms}}</td>
                  <td>{{ number_format($cliente->limcredito_dms, 2, '.', ',' ) }} $</td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
