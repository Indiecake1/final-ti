@extends('template.dashboard')
@section('title','Reporte de cobradores')
@section('pageTitle',$title)
  @section('breadcrumb')
    <li class="breadcrumb-item active" aria-current="page">Cobradores</li>
  @endsection

@section('content')
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Todos los cobradores</h5>
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                <td>ID</td>
                <td>Nombre</td>
                <td>Calle</td>
                <td>Colonia</td>
                <td>Numero de casa</td>
                <td>Ciudad</td>
                <td>Telefono</td>
                <td>Comision</td>
                <td>Fotografia</td>
              </tr>
            </thead>
            <tbody>
              @foreach ($cobradores as $cobrador)
                <tr>
                  <td>{{ $cobrador->cob_dms }}</td>
                  <td>{{ $cobrador->nombre_dms }} {{ $cobrador->apaterno_dms }} {{ $cobrador->amaterno_dms }}</td>
                  <td>{{ $cobrador->calle_dms}}</td>
                  <td>{{ $cobrador->colonia_dms }}</td>
                  <td>{{ $cobrador->nun_casa_dms}}</td>
                  <td>{{ $cobrador->Ciudad->ciudad_dms}}</td>
                  <td>{{ $cobrador->telefono_dms}}</td>
                  <td>{{ number_format($cobrador->comision_dms, 2, '.', ',' ) }} $</td>
                  <td><img width="70%" class="rounded" src="{{asset('img')}}/{{ $cobrador->fotografia_dms }}"></td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
