@extends('template.dashboard')
@section('title','Reporte de productos')
@section('pageTitle',$title)
  @section('breadcrumb')
    <li class="breadcrumb-item active" aria-current="page">Productos</li>
  @endsection

@section('content')
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Todos los productos</h5>
        <hr>

        <div class="form-group text-right">
          <button type="button" class="btn btn-success" data-toggle="modal" data-target="#nuevo"><i class="mdi mdi-library-plus"></i></button>
        </div>

        <div class="modal fade" id="nuevo" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="">Nuevo producto</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              </div>
              <div class="modal-body">
                <form class="" action="{{ route('Producto.guardar') }}" method="post">
                  @csrf
                  <input type="text" hidden="true" name="prod" value="0">
                  <div class="form-group">
                    <label for="name">Nombre del producto</label>
                    <input type="text" required class="form-control" id="name" placeholder="Nombre del producto" name="producto">
                  </div>
                  <div class="form-group">
                    <label for="cantidad">Precio</label>
                    <input type="number" required class="form-control" min="1" step="any" id="precio" name="precio" placeholder="Establezca el precio del producto">
                  </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-success">Confirmar</button>
                </form>
              </div>
            </div>
          </div>
        </div>

        <div class="responsive-table">
          <table class="table table-bordered">
            <thead>
              <tr>
                <td>Id</td>
                <td>Producto</td>
                <td>Precio</td>
                <td>Opciones</td>
              </tr>
            </thead>
            <tbody>
              @foreach ($productos as $producto)
                <tr>
                  <td>{{ $producto->prod }}</td>
                  <td>{{ $producto->producto}}</td>
                  <td>{{ number_format($producto->precio, 2, '.', ',' ) }}$</td>
                  <td>
                    <button data-toggle="modal" data-target="#editarDet{{$producto->prod}}" class="btn btn-outline-warning"><i class="mdi mdi-lead-pencil"></i></button>
                    <button data-toggle="modal" data-target="#borrar{{$producto->prod}}" class="btn btn-outline-danger"><i class="mdi mdi-delete"></i></button>
                  </td>
                </tr>

                <div class="modal fade" id="editarDet{{$producto->prod}}" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title" id="">Editar el producto <strong>{{ $producto->producto }}</strong></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      </div>
                      <div class="modal-body">
                        <form class="" action="{{ route('Producto.guardar') }}" method="post">
                          @csrf
                          <input type="text" hidden="true" name="prod" value="{{ $producto->prod }}">
                          <div class="form-group">
                            <label for="name">Nombre del producto</label>
                            <input type="text" required class="form-control" id="name" value="{{ $producto->producto }}" name="producto">
                          </div>
                          <div class="form-group">
                            <label for="cantidad">Precio</label>
                            <input type="number" required class="form-control" min="1" step="any" id="precio" name="precio" value="{{ $producto->precio }}" placeholder="Establesca el precio del producto">
                          </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-success">Confirmar</button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="modal fade" id="borrar{{$producto->prod}}" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title" id="¿Borrar registro?">¿Borrar registro?</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      </div>
                      <div class="modal-body">
                        Estas por Eliminar el producto: <strong>{{$producto->producto}}</strong>
                      </div>
                      <form class="" action="{{ route('Producto.borrar', ['id'=> $producto->prod]) }}" method="post">
                        @csrf
                        {{ method_field('DELETE') }}
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                          <button type="submit" class="btn btn-danger">Borrar</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>

              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
