<?php
/*
$css_arrays =array(
      array('ruta'=>'/assets/images/favicon.ico','type'=> 'image/x-icon', 'rel'=> 'icon'),
      array('ruta'=>'/assets/plugins/notifications/css/lobibox.min.css','rel'=> 'stylesheet'),
      array('ruta'=>'/asset/'),

  );


if(isset($css_adicionales)){ $css_arrays=array_merge($css_arrays,$css_adicionales); }
*/
?>
  <!-- Custom CSS -->
<link href={{asset('css/fullcalendar.min.css')}} rel="stylesheet" />
<link href={{asset('css/calendar.css')}} rel="stylesheet" />
<link href="{{asset('css/bootstrap.min.css')}}">
<link href="{{asset('css/style.min.css')}}" rel="stylesheet">


<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<link href="{{asset('css/datatables.css')}}">
<link href="{{asset('css/icons/material-design-iconic-font/css/materialdesignicons.min.css') }}">
<link rel="stylesheet" href="{{asset('css/select2.min.css')}}">
{{--
@for ($i = 0; $i < sizeof($css_arrays); $i++)
     <link href="{{URL::asset($css_arrays[$i]['ruta'])}}" type="{{{$css_arrays[$i]['type'] or 'text/css'}}}" rel="{{{$css_arrays[$i]['rel'] or 'stylesheet'}}}" >
@endfor
--}}
