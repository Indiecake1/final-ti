<!-- Sidebar navigation-->
<nav class="sidebar-nav">
    <ul id="sidebarnav" class="p-t-30">
        <li class="sidebar-item">
          <a class="sidebar-link waves-effect waves-dark sidebar-link" href={{ Route('index') }} aria-expanded="false">
            <i class="mdi mdi-view-dashboard"></i>
            <span class="hide-menu"> Inicio</span>
          </a>
        </li>
        <li class="sidebar-item">
          <a href="{{ Route('Pedido.inicio',['status'=>'2']) }}" class="sidebar-link">
          <i class="mdi mdi-briefcase-check"></i>
          <span class="hide-menu"> Pedidos pagados</span></a>
        </li>
        <li class="sidebar-item">
          <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
            <i class="mdi mdi-view-list"></i>
            <span class="hide-menu"> Modulos</span>
          </a>
            <ul aria-expanded="false" class="collapse  first-level">
                <li class="sidebar-item">
                  <a href={{ Route('Pedido.inicio',['status'=>'1']) }} class="sidebar-link">
                    <i class="mdi mdi-hexagon-multiple"></i><span class="hide-menu"> Pedidos pendientes </span>
                  </a>
                </li>
                <li class="sidebar-item">
                  <a href="{{ Route('iniProd') }}" class="sidebar-link">
                  <i class="mdi mdi-food-apple"></i>
                  <span class="hide-menu"> Productos</span></a>
                </li>
            </ul>
        </li>

        <li class="sidebar-item">
          <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
            <i class="mdi mdi-chart-pie"></i>
            <span class="hide-menu"> Reportes</span>
          </a>
            <ul aria-expanded="false" class="collapse  first-level">
                <li class="sidebar-item">
                  <a href="{{ Route('Reporte.sumario') }}" class="sidebar-link">
                  <i class="mdi mdi-chart-areaspline"></i>
                  <span class="hide-menu"> Sumario</span></a>
                </li>
            </ul>
        </li>

    </ul>
</nav>
<!-- End Sidebar navigation -->
