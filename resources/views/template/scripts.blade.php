<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<script src={{asset('js/jquery.min.js')}}></script>
<script src={{asset('js/jquery.ui.touch-punch-improved.js')}}></script>
<script src={{asset('js/jquery-ui.min.js')}}></script>
<!-- Bootstrap tether Core JavaScript -->
<script type="module" src={{asset('js/popper.min.js')}}></script>
<script src={{asset('js/bootstrap.min.js')}}></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src={{asset('js/perfect-scrollbar.jquery.min.js')}}></script>
<script src={{asset('js/sparkline.js')}}></script>
<!--Wave Effects -->
<script src={{asset('js/waves.js')}}></script>
<!--Menu sidebar -->
<script src={{asset('js/sidebarmenu.js')}}></script>
<!--Custom JavaScript -->
<script src={{asset('js/custom.min.js')}}></script>
<!-- this page js -->
<script src={{asset('js/moment.min.js')}}></script>
<script src={{asset('js/fullcalendar.min.js')}}></script>
<script src={{asset('js/cal-init.js')}}></script>
<script src={{asset('js/datatables.js')}}></script>
<script src={{asset('js/select2.min.js')}}></script>
