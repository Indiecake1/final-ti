@extends('template.dashboard')
@section('title','Inicio')
  @section('breadcrumb')

  @endsection

@section('content')
  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">Pedidos</h5>
          <p><strong>Pedidos pendientes</strong> <br>
            Actualmente se encuentran pendientes un total de: <strong>{{ $nPedidos }}</strong>
            pedidos.
          </p>
          <div class="text-center">
          <h6><a href="{{ Route('Pedido.inicio',['status'=>'1']) }}" class="btn btn-success btn-sm">Lista de los pedidos pendientes</a></h6>
        </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Nuevo pedido</h4>
          <div class="text-center">
              <div style="cursor:pointer;" class="col-md-6 offset-3">
                <div class="card card-hover">
                  <a class="text-white" data-toggle="modal" data-target="#nuevoPedido">
                    <div class="box bg-success text-center">
                      <h1><i class="mdi mdi-pencil text-white"></i></h1>
                    <h6><a class="text-white" data-toggle="modal" data-target="#nuevoPedido">Levantar pedido</a></h6>
                  </div>
                </a>
                </div>
              </div>
          </div>



        </div>
      </div>
    </div>
  </div>
<div class="row">
  <div class="col-md-6">
    <div class="card">
      <div class="card-body">
        <div class="aling center">
          <h3 class="card-title">Fecha: {{date('Y-m-d')}}</h3>
          <div class="text-center">
            <p>
              <img src="{{asset('img/wip.png')}}" width="60%">
            </p>
          </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="card">
        <div class="card-body">
          <div class="text-center">
            <img width="60%" class="img-fluid" src="{{asset('img/uth.png')}}" alt="">
          </div>
        </div>
      </div>
    </div>
</div>

<div class="modal fade" id="nuevoPedido" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="info">Nuevo Pedido</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <form class="form-horizontal" action="{{ route('Pedido.guardar') }}" method="post">
        @csrf
        <input type="text" hidden="true" name="folio" value="0">
        <div class="modal-body">
          <div class="form-group row">
            <label for="fname" class="col-sm-2 text-right control-label col-form-label">titular</label>
            <div class="col-sm-9">
              <input type="text" required class="form-control" id="fname" name="titular" placeholder="Ingrese su nombre">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-success">Enviar</button>

        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
$('#nuevoPedido').on('shown.bs.modal', function () {
  $('#fname').focus()
});
</script>
@endsection
