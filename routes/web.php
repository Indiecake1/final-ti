<?php

Route::get('/', 'navigationController@index')->name('index');

route::prefix('Productos')->namespace('Productos')->group(function(){
  Route::get('/Inicio', 'ProductosController@productosini')->name('iniProd');
  Route::delete('/Eliminar/{id}', 'ProductosController@eliminar')->name('Producto.borrar');
  Route::post('/Guardar', 'ProductosController@guardar')->name('Producto.guardar');
});

Route::prefix('Pedidos')->namespace('Pedidos')->group(function(){
  Route::get('Inicio/{status?}', 'PedidosController@index')->name('Pedido.inicio');
  Route::get('Detalles/{id}', 'PedidosController@show')->name('Pedido.mostrar');
  Route::get('Nuevo', 'PedidosController@create')->name('Pedido.nuevo');
  Route::get('Editar/{id}', 'PedidosController@edit')->name('Pedido.editar');
  Route::post('Guardar', 'PedidosController@store')->name('Pedido.guardar');
  Route::delete('Eliminar/{id}', 'PedidosController@destroy')->name('Pedido.borrar');
});

Route::prefix('Detalles')->namespace('Detalles')->group(function(){
  Route::get('Inicio/{status?}', 'DetallesController@index')->name('Detalle.inicio');
  Route::get('Detalles/{id}', 'DetallesController@show')->name('Detalle.mostrar');
  Route::get('Nuevo', 'DetallesController@create')->name('Detalle.nuevo');
  Route::get('Editar/{id}', 'DetallesController@edit')->name('Detalle.editar');
  Route::post('Guardar', 'DetallesController@store')->name('Detalle.guardar');
  Route::delete('Eliminar/{id}', 'DetallesController@destroy')->name('Detalle.borrar');
});

Route::prefix('Reportes')->namespace('Reportes')->group(function(){
  Route::get('Sumario','ReportesController@sumario')->name('Reporte.sumario');
  Route::get('Sumario/Exportar','ReportesController@expSumario')->name('Reporte.sumario.exportar');
});
